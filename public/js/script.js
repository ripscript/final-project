window.addEventListener('scroll', function () {
  const header = document.querySelector("header");
  const btnScrollUp = document.querySelector('a.scroll');
  header.classList.toggle("sticky", window.scrollY > 150)
  btnScrollUp.classList.toggle('scrollBtnActive', window.scrollY > 180)
})